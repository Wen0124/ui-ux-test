import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    openSideMenu: false
  },

  actions: {
    // ...
  },

  mutations: {
    SET_SIDE_MENU (state, payload) {
      state.openSideMenu = payload
    },
  }
})

export default store
